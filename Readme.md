# ROLA

Rola is UI Framework designed for desktop and mobile by follow some of Google's
Material Design guidelines.

Rola is also designed to work the way you want. Rola will not dictate style,
making developers able to extends or reuse elements using mixins, functions, etc.
